﻿using System;
using System.IO;
using System.Text.RegularExpressions;

namespace ImageCopy
{
   internal class Sorter
   {
      private ISorterOptions options;
      private string dateFormat;
      private IFileFormatReader[] fileReaders;

      public Sorter( ISorterOptions options, IFileFormatReader[] fileReaders )
      {
         this.options = options;

         switch ( this.options.DateFormat )
         {
            case DateFormat.Y:
            {
               this.dateFormat = "yyyy";
               break;
            }
            case DateFormat.YM:
            {
               this.dateFormat = "yyyy-MM";
               break;
            }
            case DateFormat.YMD:
            {
               this.dateFormat = "yyyy-MM-dd";
               break;
            }
            default:
            {
               this.dateFormat = "yyyy-MM-dd";
               break;
            }
         }

         this.fileReaders = fileReaders;
      }

      public string Sort( string sourcePath, string targetPath )
      {
         foreach ( var reader in this.fileReaders )
         {
            if ( reader.Load( sourcePath ) )
            {
               var dateTaken = reader.GetDateTaken();
               var directory = Path.Combine( Path.GetDirectoryName( targetPath ), dateTaken.ToString( this.dateFormat ) );
               return Path.Combine( directory, Path.GetFileName( targetPath ) );
            }
         }

         return targetPath;
      }
   }
}
