﻿using System;

namespace ImageCopy
{
   internal interface IFileFormatReader : IDisposable
   {
      bool Load( string path );
      DateTime GetDateTaken();
   }
}
