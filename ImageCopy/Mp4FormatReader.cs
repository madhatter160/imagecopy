﻿using System;
using System.IO;
using System.Text.RegularExpressions;

namespace ImageCopy
{
   internal class Mp4FormatReader : IFileFormatReader
   {
      private string path;
      private Regex regex = new Regex( @"\.m(p4|4v)", RegexOptions.IgnoreCase );

      public bool Load( string path )
      {
         var extension = Path.GetExtension( path );

         if ( !this.regex.IsMatch( extension ) )
         {
            return false;
         }

         this.path = path;

         return true;
      }

      public DateTime GetDateTaken()
      {
         return File.GetLastWriteTime( this.path );
      }

      #region IDisposable Support
      private bool disposedValue = false; // To detect redundant calls

      protected virtual void Dispose( bool disposing )
      {
         if ( !disposedValue )
         {
            if ( disposing )
            {
               // TODO: dispose managed state (managed objects).
            }

            // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
            // TODO: set large fields to null.

            disposedValue = true;
         }
      }

      // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
      // ~Mp4FormatReader() {
      //   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
      //   Dispose(false);
      // }

      // This code added to correctly implement the disposable pattern.
      public void Dispose()
      {
         // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
         Dispose( true );
         // TODO: uncomment the following line if the finalizer is overridden above.
         // GC.SuppressFinalize(this);
      }
      #endregion
   }
}
