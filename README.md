# ImageCopy
Copies images from a source and puts them into directories under a target directory based on specified criteria.

## Command Line Arguments
- -source: Path to the directory containing image files to copy.
- -target: Path to the directory where the images will be copied.
- -recursive: Include to recurse through subdirectories contained in -source.
- -sort: Criteria by which to sort the images.  Accepted values: DateTaken
- -dateFormat: Specifies the format of the date in the directory name.  Default is YMD.  Accepted values: Y, YM, YMD
- -overwrite: Include to automatically overwrite files in the target directory.  Default is to not overwrite.
- -noChange: Instructs the program to do everything except do the actual copy.  Useful to testing the output before actually copying files.